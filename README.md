# Cloudmore Home Assignment #

[![CucumberReports: cloudmore](https://messages.cucumber.io/api/report-collections/a0a2b319-9f0c-400a-b010-eb3d3acaecc1/badge)](https://reports.cucumber.io/report-collections/a0a2b319-9f0c-400a-b010-eb3d3acaecc1)

This README includes some UI and API test scenarios.

### Where are the scenarios? ###

* Web Desktop Scenario are located under src/resources/ui/desktop.
* Web Mobile Scenarios are located under src/resources/ui/mobile.
* API Scenarios are located under src/resources/api.

### How can I execute test scenarios? ###
Three different tags, which are @api, @desktop and @mobile, are used to separate test scenarios.
If you want to execute all together you can only execute ```mvn clean test ```.
If you want to execute only specific tags, you can execute ```mvn clean test -Dcucumber.filter.tags="@api or @mobile or @desktop"``` 


### Where are the screenshots? ###

* You can find screenshots which are generated after test execution under screenshots/desktop and screenshots/mobile folders. jpg files are ignored in .gitignore file thats why screenshots are not shared in the repo.

### Where are the reports? ###

* After each test suite execution, test report is generated and published to cucumber report page. 
You can find it from badge above or [here](https://reports.cucumber.io/report-collections/a0a2b319-9f0c-400a-b010-eb3d3acaecc1).
