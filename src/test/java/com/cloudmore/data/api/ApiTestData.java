package com.cloudmore.data.api;

import com.cloudmore.api.dto.User;
import io.restassured.response.Response;

/**
 * @author sercansensulun on 18.10.2021.
 */
public class ApiTestData {
    public User.UserBuilder userBuilder;
    public Response addUserResponse;
    public String existUsername;
    public Response deleteUserResponse;
    public String nonExistUsername;
    public Response getUserResponse;
    public User existUser;
    public Response updateUserResponse;
}
