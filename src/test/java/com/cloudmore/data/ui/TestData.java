package com.cloudmore.data.ui;

import com.cloudmore.page.desktop.*;
import com.cloudmore.page.mobile.MobileHomePageObject;
import com.cloudmore.page.mobile.MobileSearchResultPageObject;
import org.openqa.selenium.WebDriver;

/**
 * @author sercansensulun on 17.10.2021.
 */
public class TestData {
    public WebDriver desktopWebDriver;
    public WebDriver mobileWebDriver;
    public DesktopPlatformPageObject desktopPlatformPage;
    public DesktopHomePageObject desktopHomePage;
    public DesktopSolutionPageObject desktopSolutionPage;
    public DesktopAboutUsPageObject desktopAboutUsPageObject;
    public DesktopContactUsPageObject desktopContactUsPageObject;
    public DesktopBlogPageObject desktopBlogPageObject;
    public DesktopCaseStudiesPageObject desktopCaseStudiesPageObject;
    public DesktopSearchResultPageObject desktopSearchResultPageObject;
    public MobileHomePageObject mobileHomePage;
    public MobileSearchResultPageObject mobileSearchResultPageObject;
}
