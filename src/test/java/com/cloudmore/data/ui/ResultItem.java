package com.cloudmore.data.ui;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 * @author sercansensulun on 18.10.2021.
 */
@Getter
@Setter
@Builder
public class ResultItem {

    private String title;

    private String description;

    @Override
    public String toString() {
        return "ResultItem{" +
                "title='" + title + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
