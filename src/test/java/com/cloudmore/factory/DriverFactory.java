package com.cloudmore.factory;

import com.cloudmore.environment.desktop.DesktopEnvironment;
import com.cloudmore.environment.mobile.MobileEnvironment;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.commons.lang.NotImplementedException;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.NotFoundException;
import org.openqa.selenium.WebDriver;

/**
 * @author sercansensulun on 17.10.2021.
 */
public class DriverFactory {


    public static WebDriver initMobileDriver(MobileEnvironment mobileEnvironment) {
        String browserType = mobileEnvironment.getBrowserType().toLowerCase();
        int width = mobileEnvironment.getWidthSize();
        int height = mobileEnvironment.getHeightSize();
        WebDriver driver = null;
        switch(browserType)
        {
            case "chrome":
                driver = WebDriverManager.chromedriver().create();
                driver.manage().window().setSize(new Dimension(width, height));
                break;
            case "safari":
                throw new NotImplementedException();
            default:
                throw new NotFoundException();
        }
        return driver;
    }


    public static WebDriver initDesktopDriver(DesktopEnvironment desktopEnvironment) {
        String browserType = desktopEnvironment.getBrowserType().toLowerCase();
        int width = desktopEnvironment.getWidthSize();
        int height = desktopEnvironment.getHeightSize();
        WebDriver driver = null;
        switch(browserType)
        {
            case "chrome":
                driver = WebDriverManager.chromedriver().create();
                driver.manage().window().setSize(new Dimension(width, height));
                break;
            case "safari":
                throw new NotImplementedException();
            default:
                throw new NotFoundException();
        }
        return driver;
    }
}
