package com.cloudmore.api.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 * @author sercansensulun on 18.10.2021.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
@Builder
public class User {
    String username;
    String firstName;
    String lastName;
    String email;
    String password;
    String phone;
    int userStatus;
}
