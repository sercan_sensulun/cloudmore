package com.cloudmore.api.enumeration;

/**
 * @author sercansensulun on 18.10.2021.
 */
public enum ApiResource {
    USER("user");

    private final String name;

    ApiResource(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
