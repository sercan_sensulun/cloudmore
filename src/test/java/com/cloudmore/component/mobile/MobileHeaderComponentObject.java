package com.cloudmore.component.mobile;

import com.cloudmore.component.common.BaseComponentObject;
import com.cloudmore.component.desktop.DesktopSearchComponentObject;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * @author sercansensulun on 17.10.2021.
 */
public class MobileHeaderComponentObject extends BaseComponentObject {

    @FindBy(xpath = "(//*[@class=\"fa fa-search\"])[1]")
    WebElement searchButton;

    public MobileHeaderComponentObject(WebDriver driver) {
        super(driver);
    }

    public MobileSearchComponentObject clickSearchButton() {
        click(searchButton);
        return new MobileSearchComponentObject(webDriver);
    }
}
