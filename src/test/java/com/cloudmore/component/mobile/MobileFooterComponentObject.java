package com.cloudmore.component.mobile;

import com.cloudmore.component.common.BaseComponentObject;
import org.openqa.selenium.WebDriver;

/**
 * @author sercansensulun on 17.10.2021.
 */
public class MobileFooterComponentObject extends BaseComponentObject {

    public MobileFooterComponentObject(WebDriver driver) {
        super(driver);
    }
}
