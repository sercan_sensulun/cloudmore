package com.cloudmore.component.mobile;

import com.cloudmore.component.common.BaseComponentObject;
import com.cloudmore.data.ui.ResultItem;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author sercansensulun on 18.10.2021.
 */
public class MobileSearchResultBodyComponentObject extends BaseComponentObject {

    @FindBy(xpath = "//ul[@id=\"hsresults\"]//li")
    List<WebElement> resultListItems;

    @FindBy(xpath = "//a[@class=\"hs-search-results__next-page\"]")
    WebElement nextPageButton;

    @FindBy(xpath = "//a[@class=\"hs-search-results__prev-page\"]")
    WebElement previousPageButton;

    public MobileSearchResultBodyComponentObject(WebDriver driver) {
        super(driver);
    }

    public List<ResultItem> getResultItems() {
        return resultListItems.stream().map(
                webElement -> {
                    String title = webElement.findElement(By.xpath(".//a")).getText();
                    String description = webElement.findElement(By.xpath(".//p")).getText();
                    return ResultItem.builder()
                            .title(title)
                            .description(description)
                            .build();
                }
        ).collect(Collectors.toList());
    }

    public boolean isNextPageButtonDisplayed() {
        try {
            return nextPageButton.isDisplayed();
        }catch (NoSuchElementException e){
            return false;
        }
    }

    public MobileSearchResultBodyComponentObject clickNextPageButton() {
        click(nextPageButton);
        return this;
    }
}
