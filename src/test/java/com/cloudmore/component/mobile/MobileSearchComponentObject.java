package com.cloudmore.component.mobile;

import com.cloudmore.component.common.BaseComponentObject;
import com.cloudmore.page.desktop.DesktopSearchResultPageObject;
import com.cloudmore.page.mobile.MobileSearchResultPageObject;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * @author sercansensulun on 18.10.2021.
 */
public class MobileSearchComponentObject extends BaseComponentObject {

    @FindBy(xpath = "//input[@type=\"search\"]")
    WebElement searchInput;

    @FindBy(xpath = "//form[@action=\"/hs-search-results\"]//button[@type=\"submit\"]")
    WebElement searchButton;

    public MobileSearchComponentObject(WebDriver driver) {
        super(driver);
    }

    public void enterSearchKeyword(String keyword) {
        sendKeys(searchInput, keyword);
    }

    public MobileSearchResultPageObject clickSearchButton() {
        click(searchButton);
        return new MobileSearchResultPageObject(webDriver);
    }
}
