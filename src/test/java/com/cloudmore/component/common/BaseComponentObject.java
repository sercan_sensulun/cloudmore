package com.cloudmore.component.common;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;

/**
 * @author sercansensulun on 17.10.2021.
 */
public abstract class BaseComponentObject {

    protected WebDriver webDriver;

    protected WebDriverWait webDriverWait;

    public BaseComponentObject(WebDriver driver) {
        this.webDriver = driver;
        webDriverWait = new WebDriverWait(webDriver, 60);
        PageFactory.initElements(driver, this);
    }

    protected void click(WebElement webElement) {
        webDriverWait.until(ExpectedConditions.visibilityOf(webElement));
        webDriverWait.until(ExpectedConditions.elementToBeClickable(webElement));
        JavascriptExecutor jse = (JavascriptExecutor) webDriver;
        jse.executeScript("arguments[0].click()", webElement);
    }

    protected boolean isVisible(WebElement webElement) {
        webDriverWait.until(ExpectedConditions.visibilityOf(webElement));
        return true;
    }

    protected void sendKeys(WebElement webElement, String keyword) {
        webDriverWait.until(ExpectedConditions.visibilityOf(webElement));
        webElement.sendKeys(keyword);
    }

    public void takeScreenShot(String folderName) {
        waitUntilPageLoad();
        String path = "screenshots/%s.jpg";
        if (!StringUtils.isEmpty(folderName)) {
            path = "screenshots/" + folderName + "/%s.jpg";
        }
        TakesScreenshot takesScreenshot = (TakesScreenshot) webDriver;
        File screenShotFile = takesScreenshot.getScreenshotAs(OutputType.FILE);
        File destination = new File(String.format(path, System.currentTimeMillis()));
        try {
            FileUtils.copyFile(screenShotFile, destination);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected void waitUntilPageLoad() {
        webDriverWait.until(webDriver -> ((JavascriptExecutor) webDriver).executeScript("return document.readyState").equals("complete"));
    }
}
