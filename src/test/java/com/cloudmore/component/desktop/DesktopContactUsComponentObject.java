package com.cloudmore.component.desktop;

import com.cloudmore.component.common.BaseComponentObject;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * @author sercansensulun on 18.10.2021.
 */
public class DesktopContactUsComponentObject extends BaseComponentObject {

    @FindBy(xpath = "//div[@class=\"body-container-wrapper\"]//input[@name=\"firstname\"]")
    WebElement firstNameInput;

    @FindBy(xpath = "//div[@class=\"body-container-wrapper\"]//input[@name=\"lastname\"]")
    WebElement lastNameInput;

    @FindBy(xpath = "//div[@class=\"body-container-wrapper\"]//input[@name=\"email\"]")
    WebElement emailInput;

    @FindBy(xpath = "//div[@class=\"body-container-wrapper\"]//input[@name=\"phone\"]")
    WebElement phoneInput;

    @FindBy(xpath = "//div[@class=\"body-container-wrapper\"]//textarea[@name=\"message\"]")
    WebElement messageInput;

    @FindBy(xpath = "//div[@class=\"body-container-wrapper\"]//input[@type=\"submit\"]")
    WebElement submitButton;

    public DesktopContactUsComponentObject(WebDriver driver) {
        super(driver);
    }

    public boolean isFirstNameFieldVisible() {
        return isVisible(firstNameInput);
    }

    public boolean isLastNameFieldVisible() {
        return isVisible(lastNameInput);
    }

    public boolean isEmailFieldVisible() {
        return isVisible(emailInput);
    }

    public boolean isMessageFieldVisible() {
        return isVisible(messageInput);
    }

    public boolean isSubmitButtonVisible() {
        return isVisible(submitButton);
    }
}
