package com.cloudmore.component.desktop;

import com.cloudmore.component.common.BaseComponentObject;
import com.cloudmore.page.desktop.DesktopSearchResultPageObject;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * @author sercansensulun on 17.10.2021.
 */
public class DesktopSearchComponentObject extends BaseComponentObject {

    @FindBy(xpath = "//input[@type=\"search\"]")
    WebElement searchInput;

    @FindBy(xpath = "//form[@action=\"/hs-search-results\"]//button[@type=\"submit\"]")
    WebElement searchButton;

    public DesktopSearchComponentObject(WebDriver driver) {
        super(driver);
    }

    public void enterSearchKeyword(String keyword) {
        sendKeys(searchInput, keyword);
    }

    public DesktopSearchResultPageObject clickSearchButton() {
        click(searchButton);
        return new DesktopSearchResultPageObject(webDriver);
    }
}
