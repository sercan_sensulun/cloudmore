package com.cloudmore.component.desktop;

import com.cloudmore.component.common.BaseComponentObject;
import com.cloudmore.page.desktop.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * @author sercansensulun on 17.10.2021.
 */
public class DesktopHeaderComponentObject extends BaseComponentObject {

    @FindBy(xpath = "(//a[@href=\"https://web.cloudmore.com/product\"])[1]")
    WebElement platformMenuItem;

    @FindBy(xpath = "(//a[@href=\"https://web.cloudmore.com/solutions\"])[1]")
    WebElement solutionMenuItem;

    @FindBy(xpath = "(//a[@href=\"https://web.cloudmore.com/about-us\"])[1]")
    WebElement aboutUsMenuItem;

    @FindBy(xpath = "(//a[@href=\"https://web.cloudmore.com/contact-us\"])[1]")
    WebElement contactUsMenuItem;

    @FindBy(xpath = "(//a[@href=\"https://web.cloudmore.com/blog\"])[1]")
    WebElement blogMenuItem;

    @FindBy(xpath = "(//a[@href=\"https://web.cloudmore.com/case-studies\"])[1]")
    WebElement caseStudiesMenuItem;

    @FindBy(xpath = "(//a[@href=\"https://web.cloudmore.com\"]//img)[1]")
    WebElement companyLogo;

    @FindBy(xpath = "(//*[@class=\"fa fa-search\"])[1]")
    WebElement searchButton;


    public DesktopHeaderComponentObject(WebDriver driver) {
        super(driver);
    }

    public DesktopPlatformPageObject clickPlatformMenuItem() {
        click(platformMenuItem);
        return new DesktopPlatformPageObject(webDriver);
    }

    public DesktopSolutionPageObject clickSolutionsMenuItem() {
        click(solutionMenuItem);
        return new DesktopSolutionPageObject(webDriver);
    }

    public DesktopAboutUsPageObject clickAboutUsMenuItem() {
        click(aboutUsMenuItem);
        return new DesktopAboutUsPageObject(webDriver);
    }

    public DesktopContactUsPageObject clickContactUsMenuItem() {
        click(contactUsMenuItem);
        return new DesktopContactUsPageObject(webDriver);
    }

    public DesktopBlogPageObject clickBlogMenuItem() {
        click(blogMenuItem);
        return new DesktopBlogPageObject(webDriver);
    }

    public DesktopCaseStudiesPageObject clickCaseStudiesMenuItem() {
        click(caseStudiesMenuItem);
        return new DesktopCaseStudiesPageObject(webDriver);
    }

    public void clickHamburgerMenuButton() {

    }

    public DesktopSearchComponentObject clickSearchButton() {
        click(searchButton);
        return new DesktopSearchComponentObject(webDriver);
    }

    public boolean isCompanyLogoVisible() {
        return isVisible(companyLogo);
    }

    public boolean isBlogMenuItemVisible(){
        return isVisible(blogMenuItem);
    }

    public boolean isPlatformMenuItemVisible(){
        return isVisible(platformMenuItem);
    }

    public boolean isSolutionsMenuItemVisible(){
        return isVisible(solutionMenuItem);
    }

    public boolean isAboutUsMenuItemVisible(){
        return isVisible(aboutUsMenuItem);
    }

    public boolean isContactUsMenuItemVisible(){
        return isVisible(contactUsMenuItem);
    }

    public boolean isCaseStudiesMenuItemVisible(){
        return isVisible(caseStudiesMenuItem);
    }
}
