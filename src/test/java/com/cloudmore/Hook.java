package com.cloudmore;

import com.cloudmore.data.api.ApiTestData;
import com.cloudmore.data.ui.TestData;
import com.cloudmore.environment.ApiEnvironment;
import com.cloudmore.environment.api.LocalFileAPIEnvironment;
import com.cloudmore.environment.desktop.DesktopEnvironment;
import com.cloudmore.environment.desktop.LocalFileDesktopEnvironment;
import com.cloudmore.environment.mobile.LocalFileMobileEnvironment;
import com.cloudmore.environment.mobile.MobileEnvironment;
import com.cloudmore.factory.DriverFactory;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.restassured.RestAssured;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;

import java.io.IOException;
import java.util.UUID;

/**
 * @author sercansensulun on 17.10.2021.
 */
public class Hook {

    private TestData testData;

    private ApiTestData apiTestData;

    public Hook(TestData testData, ApiTestData apiTestData) {
        this.testData = testData;
        this.apiTestData = apiTestData;
    }

    @Before("@desktop")
    public void beforeDesktop() throws IOException {
        DesktopEnvironment desktopEnvironment = new LocalFileDesktopEnvironment("properties/desktop/environment.properties");
        testData.desktopWebDriver = DriverFactory.initDesktopDriver(desktopEnvironment);
    }

    @Before("@mobile")
    public void beforeMobile() throws IOException {
        MobileEnvironment mobileEnvironment = new LocalFileMobileEnvironment("properties/mobile/environment.properties");
        testData.mobileWebDriver = DriverFactory.initMobileDriver(mobileEnvironment);
    }

    @Before("@api")
    public void beforeApi() throws IOException {
        ApiEnvironment apiEnvironment = new LocalFileAPIEnvironment("properties/api/environment.properties");
        RestAssured.baseURI = apiEnvironment.getApiBaseURL();
        if (apiEnvironment.isLogEnabled()) {
            RestAssured.filters(new RequestLoggingFilter(), new ResponseLoggingFilter());
        }
        //we can also validate non exist user from db.
        apiTestData.nonExistUsername = UUID.randomUUID().toString();
    }

    @After("@api")
    public void afterApi() {
        //delete test data here.
    }

    @After("@mobile")
    public void afterMobile() {
        testData.mobileWebDriver.quit();
    }

    @After("@desktop")
    public void afterDesktop() {
        testData.desktopWebDriver.quit();
    }
}
