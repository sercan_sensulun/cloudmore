package com.cloudmore.stepdefs.desktop;

import com.cloudmore.data.ui.TestData;
import com.cloudmore.page.desktop.DesktopContactUsPageObject;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.jupiter.api.Assertions;

/**
 * @author sercansensulun on 17.10.2021.
 */
public class DesktopContactUsStepdefs {

    private TestData testData;

    public DesktopContactUsStepdefs(TestData testData) {
        this.testData = testData;
        this.testData.desktopContactUsPageObject = new DesktopContactUsPageObject(testData.desktopWebDriver);
    }

    @Then("Contact us form is shown in body of desktop contact us page")
    public void contactUsFormIsShownInBodyOfDesktopContactUsPage() {
        Assertions.assertTrue(this.testData.desktopContactUsPageObject.isBodyContactUsFormVisible());
    }

    @When("desktop contact us page is opened")
    public void desktopContactUsPageIsOpened() {
        this.testData.desktopContactUsPageObject.open();
    }

    @Then("Contact us form is shown in footer of desktop contact us page")
    public void contactUsFormIsShownInFooterOfDesktopContactUsPage() {
        Assertions.assertTrue(this.testData.desktopContactUsPageObject.isFooterContactUsFormVisible());
    }
}
