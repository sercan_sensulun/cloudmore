package com.cloudmore.stepdefs.desktop;

import com.cloudmore.data.ui.ResultItem;
import com.cloudmore.data.ui.TestData;
import com.cloudmore.page.desktop.DesktopSearchResultPageObject;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Assertions;

import java.util.List;

/**
 * @author sercansensulun on 17.10.2021.
 */
@Slf4j
public class DesktopSearchStepdefs {

    private TestData testData;

    public DesktopSearchStepdefs(TestData testData) {
        this.testData = testData;
    }

    @When("{string} keyword is searched on desktop home page")
    public void keywordIsSearchedOnDesktopHomePage(String keyword) {
        DesktopSearchResultPageObject desktopSearchResultPageObject = this.testData.desktopHomePage.search(keyword);
        Assertions.assertNotNull(desktopSearchResultPageObject);
        this.testData.desktopSearchResultPageObject = desktopSearchResultPageObject;
    }

    @Then("each result has {string} keyword on desktop search result page")
    public void eachResultHasKeywordOnDesktopSearchResultPage(String keyword) {
        DesktopSearchResultPageObject desktopSearchResultPageObject = this.testData.desktopSearchResultPageObject;
        List<ResultItem> resultItemList = desktopSearchResultPageObject.getResultItems();
        Assertions.assertTrue(resultItemList.size() > 0);
        resultItemList.forEach(
                resultItem -> {
                    boolean isResultShown = StringUtils.containsIgnoreCase(resultItem.getTitle(), keyword) || StringUtils.containsIgnoreCase(resultItem.getDescription(), keyword);
                    Assertions.assertTrue(isResultShown, String.format("Result item is %s. And search keyword is %s", resultItem.toString(), keyword));
                }
        );
    }
}
