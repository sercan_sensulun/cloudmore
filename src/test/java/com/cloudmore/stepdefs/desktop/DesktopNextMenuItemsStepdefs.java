package com.cloudmore.stepdefs.desktop;

import com.cloudmore.data.ui.TestData;
import io.cucumber.java.en.Then;
import org.junit.jupiter.api.Assertions;

/**
 * @author sercansensulun on 17.10.2021.
 */
public class DesktopNextMenuItemsStepdefs {

    private TestData testData;

    public DesktopNextMenuItemsStepdefs(TestData testData){
        this.testData = testData;
    }
    
    @Then("platform menu item is shown on desktop home page")
    public void platformMenuItemIsShownOnDesktopHomePage() {
        Assertions.assertTrue(this.testData.desktopHomePage.isPlatformMenuItemVisible());
    }

    @Then("solution menu item is shown on desktop home page")
    public void solutionMenuItemIsShownOnDesktopHomePage() {
        Assertions.assertTrue(this.testData.desktopHomePage.isSolutionMenuItemVisible());
    }

    @Then("about us menu item is shown on desktop home page")
    public void aboutUsMenuItemIsShownOnDesktopHomePage() {
        Assertions.assertTrue(this.testData.desktopHomePage.isAboutUsMenuItemVisible());
    }

    @Then("contact us menu item is shown on desktop home page")
    public void contactUsMenuItemIsShownOnDesktopHomePage() {
        Assertions.assertTrue(this.testData.desktopHomePage.isContactUsMenuItemVisible());
    }

    @Then("blog menu item is shown on desktop home page")
    public void blogMenuItemIsShownOnDesktopHomePage() {
        Assertions.assertTrue(this.testData.desktopHomePage.isBlogMenuItemVisible());
    }

    @Then("case studies menu item is shown on desktop home page")
    public void caseStudiesMenuItemIsShownOnDesktopHomePage() {
        Assertions.assertTrue(this.testData.desktopHomePage.isCaseStudiesMenuItemVisible());
    }
}
