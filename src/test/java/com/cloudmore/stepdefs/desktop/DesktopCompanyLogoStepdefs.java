package com.cloudmore.stepdefs.desktop;

import com.cloudmore.data.ui.TestData;
import com.cloudmore.page.desktop.*;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.jupiter.api.Assertions;

/**
 * @author sercansensulun on 17.10.2021.
 */
public class DesktopCompanyLogoStepdefs {

    private TestData testData;

    public DesktopCompanyLogoStepdefs(TestData testData) {
        this.testData = testData;
        this.testData.desktopHomePage = new DesktopHomePageObject(testData.desktopWebDriver);
    }

    @When("desktop home page is opened")
    public void desktopHomePageIsOpened() {
        this.testData.desktopHomePage.open();
    }

    @Then("company logo is shown on desktop home page")
    public void companyLogoIsShownOnDesktopHomePage() {
        Assertions.assertTrue(this.testData.desktopHomePage.isCompanyLogoVisible());
    }

    @When("platform menu item is clicked on desktop home page")
    public void platformMenuItemIsClickedOnDesktopHomePage() {
        DesktopPlatformPageObject desktopPlatformPageObject = this.testData.desktopHomePage.clickPlatformMenuItem();
        Assertions.assertNotNull(desktopPlatformPageObject);
        this.testData.desktopPlatformPage = desktopPlatformPageObject;
    }

    @Then("company logo is shown on desktop platform page")
    public void companyLogoIsShownOnDesktopPlatformPage() {
        DesktopPlatformPageObject desktopPlatformPage = this.testData.desktopPlatformPage;
        Assertions.assertTrue(desktopPlatformPage.isCompanyLogoVisible());
    }

    @When("solution menu item is clicked on desktop home page")
    public void solutionMenuItemIsClickedOnDesktopHomePage() {
        DesktopSolutionPageObject desktopSolutionPageObject = this.testData.desktopHomePage.clickSolutionMenuItem();
        Assertions.assertNotNull(desktopSolutionPageObject);
        this.testData.desktopSolutionPage = desktopSolutionPageObject;
    }

    @Then("company logo is shown on desktop solution page")
    public void companyLogoIsShownOnDesktopSolutionPage() {
        DesktopSolutionPageObject desktopSolutionPageObject = this.testData.desktopSolutionPage;
        Assertions.assertTrue(desktopSolutionPageObject.isCompanyLogoVisible());
    }

    @When("about us menu item is clicked on desktop home page")
    public void aboutUsMenuItemIsClickedOnDesktopHomePage() {
        DesktopAboutUsPageObject desktopAboutUsPageObject = this.testData.desktopHomePage.clickAboutUsMenuItem();
        Assertions.assertNotNull(desktopAboutUsPageObject);
        this.testData.desktopAboutUsPageObject = desktopAboutUsPageObject;
    }

    @Then("company logo is shown on desktop about us page")
    public void companyLogoIsShownOnDesktopAboutUsPage() {
        DesktopAboutUsPageObject desktopAboutUsPageObject = this.testData.desktopAboutUsPageObject;
        Assertions.assertTrue(desktopAboutUsPageObject.isCompanyLogoVisible());
    }

    @When("contact us menu item is clicked on desktop home page")
    public void contactUsMenuItemIsClickedOnDesktopHomePage() {
        DesktopContactUsPageObject desktopContactUsPageeObject = this.testData.desktopHomePage.clickContactUsMenuItem();
        Assertions.assertNotNull(desktopContactUsPageeObject);
        this.testData.desktopContactUsPageObject = desktopContactUsPageeObject;
    }

    @Then("company logo is shown on desktop contact us page")
    public void companyLogoIsShownOnDesktopContactUsPage() {
        DesktopContactUsPageObject desktopContactUsPageeObject = this.testData.desktopContactUsPageObject;
        Assertions.assertTrue(desktopContactUsPageeObject.isCompanyLogoVisible());
    }

    @When("blog menu item is clicked on desktop home page")
    public void blogMenuItemIsClickedOnDesktopHomePage() {
        DesktopBlogPageObject desktopBlogPageObject = this.testData.desktopHomePage.clickBlogMenuItem();
        Assertions.assertNotNull(desktopBlogPageObject);
        this.testData.desktopBlogPageObject = desktopBlogPageObject;
    }

    @Then("company logo is shown on desktop blog page")
    public void companyLogoIsShownOnDesktopBlogPage() {
        DesktopBlogPageObject desktopBlogPageObject = this.testData.desktopHomePage.clickBlogMenuItem();
        Assertions.assertTrue(desktopBlogPageObject.isCompanyLogoVisible());
    }

    @When("case studies menu item is clicked on desktop home page")
    public void caseStudiesMenuItemIsClickedOnDesktopHomePage() {
        DesktopCaseStudiesPageObject desktopCaseStudiesPageObject = this.testData.desktopHomePage.clickCaseStudiesMenuItem();
        Assertions.assertNotNull(desktopCaseStudiesPageObject);
        this.testData.desktopCaseStudiesPageObject = desktopCaseStudiesPageObject;
    }

    @Then("company logo is shown on desktop case studies page")
    public void companyLogoIsShownOnDesktopCaseStudiesPage() {
        DesktopCaseStudiesPageObject desktopCaseStudiesPageObject = this.testData.desktopCaseStudiesPageObject;
        Assertions.assertTrue(desktopCaseStudiesPageObject.isCompanyLogoVisible());
    }

    @And("desktop platform page is opened successfully with {string} url")
    public void desktopPlatformPageIsOpenedSuccessfullyWithUrl(String expectedURL) {
        DesktopPlatformPageObject desktopPlatformPage = this.testData.desktopPlatformPage;
        String url = desktopPlatformPage.getURL();
        Assertions.assertEquals(expectedURL, url);
    }

    @And("desktop solution page is opened successfully with {string} url")
    public void desktopSolutionPageIsOpenedSuccessfullyWithUrl(String expectedURL) {
        String url = testData.desktopSolutionPage.getURL();
        Assertions.assertEquals(expectedURL, url);
    }

    @And("desktop about us page is opened successfully with {string} url")
    public void desktopAboutUsPageIsOpenedSuccessfullyWithUrl(String expectedURL) {
        String url = this.testData.desktopAboutUsPageObject.getURL();
        Assertions.assertEquals(expectedURL, url);
    }

    @And("desktop contact us page is opened successfully with {string} url")
    public void desktopContactUsPageIsOpenedSuccessfullyWithUrl(String expectedURL) {
        DesktopContactUsPageObject desktopContactUsPageeObject = this.testData.desktopContactUsPageObject;
        String url = desktopContactUsPageeObject.getURL();
        Assertions.assertEquals(expectedURL, url);
    }

    @And("desktop blog page is opened successfully with {string} url")
    public void desktopBlogPageIsOpenedSuccessfullyWithUrl(String expectedURL) {
        DesktopBlogPageObject desktopBlogPageObject = this.testData.desktopHomePage.clickBlogMenuItem();
        String url = desktopBlogPageObject.getURL();
        Assertions.assertEquals(expectedURL, url);
    }

    @And("desktop case studies page is opened successfully with {string} url")
    public void desktopCaseStudiesPageIsOpenedSuccessfullyWithUrl(String expectedURL) {
        DesktopCaseStudiesPageObject desktopCaseStudiesPageObject = this.testData.desktopCaseStudiesPageObject;
        String url = desktopCaseStudiesPageObject.getURL();
        Assertions.assertEquals(expectedURL, url);
    }
}
