package com.cloudmore.stepdefs.api;

import com.cloudmore.api.dto.ApiResponse;
import com.cloudmore.api.dto.User;
import com.cloudmore.api.enumeration.ApiResource;
import com.cloudmore.data.api.ApiTestData;
import com.cloudmore.stepdefs.ApiStepDefs;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.junit.jupiter.api.Assertions;

import java.util.UUID;

import static io.restassured.RestAssured.given;

/**
 * @author sercansensulun on 18.10.2021.
 */
public class ApiUserCreateStepdefs extends ApiStepDefs {

    public ApiUserCreateStepdefs(ApiTestData testData) {
        super(testData);
        testData.userBuilder = User.builder();
    }

    @Given("{string} username is entered")
    public void usernameIsEntered(String username) {
        testData.userBuilder.username(username);
    }

    @And("{string} first name is entered")
    public void firstNameIsEntered(String firstName) {
        testData.userBuilder.firstName(firstName);
    }

    @And("{string} last name is entered")
    public void lastNameIsEntered(String lastName) {
        testData.userBuilder.lastName(lastName);
    }

    @And("{string} e-mail is entered")
    public void eMailIsEntered(String email) {
        testData.userBuilder.email(email);
    }

    @And("{string} phone number is entered")
    public void phoneNumberIsEntered(String phone) {
        testData.userBuilder.phone(phone);
    }

    @And("{string} password is entered")
    public void passwordIsEntered(String password) {
        testData.userBuilder.password(password);
    }

    @Given("there is user with {string} username")
    public void thereIsUserWithUsername(String username) {
        Response getUserResponse = given()
                .contentType(ContentType.JSON)
                .pathParam("username", username)
                .when()
                .get(ApiResource.USER.getName() + "/{username}");
        if (getUserResponse.getStatusCode() == 200) {
            testData.existUser = getUserResponse.then().extract().as(User.class);
        } else {
            User user = User.builder()
                    .username(username)
                    .password(UUID.randomUUID().toString())
                    .phone("905350000000")
                    .lastName(UUID.randomUUID().toString())
                    .firstName(UUID.randomUUID().toString())
                    .email(UUID.randomUUID().toString() + "@yopmail.com")
                    .build();
            testData.existUser = given()
                    .contentType(ContentType.JSON)
                    .body(user)
                    .post(ApiResource.USER.getName())
                    .then()
                    .statusCode(200)
                    .extract()
                    .as(User.class);
        }
    }

    @When("user is tried to be added via API")
    public void userIsTriedToBeAddedViaAPI() {
        User user = testData.userBuilder.build();
        testData.addUserResponse = given()
                .contentType(ContentType.JSON)
                .when()
                .body(user)
                .post(ApiResource.USER.getName());
    }

    @Then("{string} response code is returned in add response")
    public void responseCodeIsReturnedInAddResponse(String expectedCode) {
        Response addResponse = testData.addUserResponse;
        Assertions.assertEquals(Integer.parseInt(expectedCode), addResponse.getStatusCode());
    }

    @And("{string} message is returned in add response")
    public void messageIsReturnedInAddResponse(String expectedMessage) {
        Response addResponse = testData.addUserResponse;
        ApiResponse apiResponse = addResponse.then().extract().as(ApiResponse.class);
        Assertions.assertEquals(expectedMessage, apiResponse.getMessage());
    }
}
