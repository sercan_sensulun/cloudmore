package com.cloudmore.stepdefs.api;

import com.cloudmore.api.enumeration.ApiResource;
import com.cloudmore.data.api.ApiTestData;
import com.cloudmore.stepdefs.ApiStepDefs;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.junit.jupiter.api.Assertions;

import java.util.UUID;

import static io.restassured.RestAssured.given;

/**
 * @author sercansensulun on 18.10.2021.
 */
public class ApiUserDeleteStepdefs extends ApiStepDefs {

    public ApiUserDeleteStepdefs(ApiTestData testData) {
        super(testData);
    }

    @When("exist username is entered to delete on via API")
    public void existUsernameIsEnteredToDeleteOnViaAPI() {
        testData.deleteUserResponse = given()
                .contentType(ContentType.JSON)
                .pathParam("username", testData.existUser.getUsername())
                .when()
                .delete(ApiResource.USER.getName() + "/{username}");
    }

    @When("{string} username is entered to delete via API")
    public void usernameIsEnteredToDeleteViaAPI(String username) {
        testData.deleteUserResponse = given()
                .contentType(ContentType.JSON)
                .pathParam("username", username)
                .when()
                .delete(ApiResource.USER.getName() + "/{username}");
    }

    @When("non-exist username is entered to delete via API")
    public void nonExistUsernameIsEnteredToDeleteViaAPI() {
        testData.deleteUserResponse = given()
                .contentType(ContentType.JSON)
                .pathParam("username", testData.nonExistUsername)
                .when()
                .delete(ApiResource.USER.getName() + "/{username}");
    }

    @Then("{string} response code is returned in delete response")
    public void responseCodeIsReturnedInDeleteResponse(String expectedCode) {
        Response deleteUserResponse = testData.deleteUserResponse;
        Assertions.assertEquals(Integer.parseInt(expectedCode), deleteUserResponse.getStatusCode());
    }
}
