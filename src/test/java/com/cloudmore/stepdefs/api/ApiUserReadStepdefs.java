package com.cloudmore.stepdefs.api;

import com.cloudmore.api.dto.User;
import com.cloudmore.api.enumeration.ApiResource;
import com.cloudmore.data.api.ApiTestData;
import com.cloudmore.stepdefs.ApiStepDefs;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.junit.jupiter.api.Assertions;

import static io.restassured.RestAssured.given;

/**
 * @author sercansensulun on 18.10.2021.
 */
public class ApiUserReadStepdefs extends ApiStepDefs {

    public ApiUserReadStepdefs(ApiTestData testData) {
        super(testData);
    }

    @When("{string} username is entered to get user via API")
    public void usernameIsEnteredToGetUserViaAPI(String username) {
        testData.getUserResponse = given()
                .contentType(ContentType.JSON)
                .pathParam("username", username)
                .when()
                .get(ApiResource.USER.getName() + "/{username}");
    }

    @Then("{string} response code is returned in get response")
    public void responseCodeIsReturnedInGetResponse(String expectedCode) {
        Response getUserResponse = testData.getUserResponse;
        Assertions.assertEquals(Integer.parseInt(expectedCode), getUserResponse.getStatusCode());
    }

    @When("non-exist username is entered to get user via API")
    public void nonExistUsernameIsEnteredToGetUserViaAPI() {
        testData.getUserResponse = given()
                .contentType(ContentType.JSON)
                .pathParam("username", testData.nonExistUsername)
                .when()
                .get(ApiResource.USER.getName() + "/{username}");
    }

    @And("username is returned in get response")
    public void usernameIsReturnedInGetResponse() {
        User actualUser = testData.getUserResponse.then().extract().as(User.class);
        User expectedUser = testData.existUser;
        Assertions.assertEquals(expectedUser.getUsername(), actualUser.getUsername());
    }

    @And("first name is returned in get response")
    public void firstNameIsReturnedInGetResponse() {
        User actualUser = testData.getUserResponse.then().extract().as(User.class);
        User expectedUser = testData.existUser;
        Assertions.assertEquals(expectedUser.getFirstName(), actualUser.getFirstName());
    }

    @And("last name is returned in get response")
    public void lastNameIsReturnedInGetResponse() {
        User actualUser = testData.getUserResponse.then().extract().as(User.class);
        User expectedUser = testData.existUser;
        Assertions.assertEquals(expectedUser.getLastName(), actualUser.getLastName());
    }
}
