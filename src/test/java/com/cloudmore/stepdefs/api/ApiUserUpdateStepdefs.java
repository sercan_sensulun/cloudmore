package com.cloudmore.stepdefs.api;

import com.cloudmore.api.dto.User;
import com.cloudmore.api.enumeration.ApiResource;
import com.cloudmore.data.api.ApiTestData;
import com.cloudmore.stepdefs.ApiStepDefs;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.junit.jupiter.api.Assertions;

import static io.restassured.RestAssured.given;

/**
 * @author sercansensulun on 18.10.2021.
 */
public class ApiUserUpdateStepdefs extends ApiStepDefs {

    public ApiUserUpdateStepdefs(ApiTestData testData) {
        super(testData);
    }

    @When("{string} username is entered to update user via API")
    public void usernameIsEnteredToUpdateUserViaAPI(String username) {
        User user = testData.userBuilder.build();
        testData.updateUserResponse = given()
                .contentType(ContentType.JSON)
                .body(user)
                .pathParam("username", username)
                .when()
                .put(ApiResource.USER.getName() + "/{username}");
    }

    @Then("{string} response code is returned in update response")
    public void responseCodeIsReturnedInUpdateResponse(String expectedCode) {
        Response updateUserResponse = testData.updateUserResponse;
        Assertions.assertEquals(Integer.parseInt(expectedCode), updateUserResponse.getStatusCode());
    }

    @When("non-exist username is entered to update user via API")
    public void nonExistUsernameIsEnteredToUpdateUserViaAPI() {
        User user = testData.userBuilder.build();
        testData.updateUserResponse = given()
                .contentType(ContentType.JSON)
                .body(user)
                .pathParam("username", testData.nonExistUsername)
                .when()
                .put(ApiResource.USER.getName() + "/{username}");
    }
}
