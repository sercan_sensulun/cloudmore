package com.cloudmore.stepdefs.mobile;

import com.cloudmore.data.ui.ResultItem;
import com.cloudmore.data.ui.TestData;
import com.cloudmore.page.mobile.MobileHomePageObject;
import com.cloudmore.page.mobile.MobileSearchResultPageObject;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Assertions;

import java.util.List;

/**
 * @author sercansensulun on 17.10.2021.
 */
public class MobileSearchStepdefs {

    private TestData testData;

    public MobileSearchStepdefs(TestData testData){
        this.testData = testData;
        this.testData.mobileHomePage = new MobileHomePageObject(testData.mobileWebDriver);
    }

    @Given("mobile home page is opened")
    public void mobileHomePageIsOpened() {
        this.testData.mobileHomePage.open();
    }

    @When("{string} keyword is searched on mobile home page")
    public void keywordIsSearchedOnMobileHomePage(String keyword) {
        MobileSearchResultPageObject mobileSearchResultPageObject = this.testData.mobileHomePage.search(keyword);
        Assertions.assertNotNull(mobileSearchResultPageObject);
        this.testData.mobileSearchResultPageObject = mobileSearchResultPageObject;
    }

    @Then("each result has {string} keyword on mobile search result page")
    public void eachResultHasKeywordOnMobileSearchResultPage(String keyword) {
        MobileSearchResultPageObject mobileSearchResultPageObject = this.testData.mobileSearchResultPageObject;
        List<ResultItem> resultItemList = mobileSearchResultPageObject.getResultItems();
        Assertions.assertTrue(resultItemList.size() > 0);
        resultItemList.forEach(
                resultItem -> {
                    boolean isResultShown = StringUtils.containsIgnoreCase(resultItem.getTitle(), keyword) || StringUtils.containsIgnoreCase(resultItem.getDescription(), keyword);
                    Assertions.assertTrue(isResultShown, String.format("Result item is %s. And search keyword is %s", resultItem.toString(), keyword));
                }
        );
    }
}
