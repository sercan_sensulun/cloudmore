package com.cloudmore.environment.desktop;

import com.cloudmore.environment.AbstractLocalFileEnvironment;

import java.io.IOException;

/**
 * @author sercansensulun on 17.10.2021.
 */
public class LocalFileDesktopEnvironment extends AbstractLocalFileEnvironment implements DesktopEnvironment {

    public LocalFileDesktopEnvironment(String filePath) throws IOException {
        super.loadProperties(filePath);
    }

    @Override
    public String getCloudmoreUIUrl() {
        return properties.getProperty("cloudmore.ui.url");
    }

    @Override
    public String getBrowserType() {
        return properties.getProperty("desktop.browser.type");
    }

    @Override
    public int getWidthSize() {
        return Integer.parseInt(properties.getProperty("desktop.page.width"));
    }

    @Override
    public int getHeightSize() {
        return Integer.parseInt(properties.getProperty("desktop.page.height"));
    }
}
