package com.cloudmore.environment.desktop;

import com.cloudmore.environment.UIEnvironment;

/**
 * @author sercansensulun on 17.10.2021.
 */
public interface DesktopEnvironment extends UIEnvironment {
}
