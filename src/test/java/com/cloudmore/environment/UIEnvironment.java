package com.cloudmore.environment;

/**
 * @author sercansensulun on 17.10.2021.
 */
public interface UIEnvironment {

    String getCloudmoreUIUrl();

    String getBrowserType();

    int getWidthSize();

    int getHeightSize();
}
