package com.cloudmore.environment;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

/**
 * @author sercansensulun on 17.10.2021.
 */
public abstract class AbstractLocalFileEnvironment {

    protected Properties properties;

    protected void loadProperties(String filePath) throws IOException {
        this.properties = new Properties();
        BufferedReader reader = new BufferedReader(new FileReader(filePath));
        properties.load(reader);
    }
}
