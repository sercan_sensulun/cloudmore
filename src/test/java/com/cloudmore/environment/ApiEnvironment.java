package com.cloudmore.environment;

/**
 * @author sercansensulun on 18.10.2021.
 */
public interface ApiEnvironment {

    String getApiBaseURL();

    boolean isLogEnabled();
}
