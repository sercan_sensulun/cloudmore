package com.cloudmore.environment.mobile;

import com.cloudmore.environment.UIEnvironment;

/**
 * @author sercansensulun on 17.10.2021.
 */
public interface MobileEnvironment extends UIEnvironment {
}
