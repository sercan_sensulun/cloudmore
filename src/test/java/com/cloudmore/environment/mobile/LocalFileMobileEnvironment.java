package com.cloudmore.environment.mobile;

import com.cloudmore.environment.AbstractLocalFileEnvironment;

import java.io.IOException;

/**
 * @author sercansensulun on 17.10.2021.
 */
public class LocalFileMobileEnvironment extends AbstractLocalFileEnvironment implements MobileEnvironment {

    public LocalFileMobileEnvironment(String filePath) throws IOException {
        super.loadProperties(filePath);
    }

    @Override
    public String getCloudmoreUIUrl() {
        return properties.getProperty("cloudmore.ui.url");
    }

    @Override
    public String getBrowserType() {
        return properties.getProperty("mobile.browser.type");
    }

    @Override
    public int getWidthSize() {
        return Integer.parseInt(properties.getProperty("mobile.page.width"));
    }

    @Override
    public int getHeightSize() {
        return Integer.parseInt(properties.getProperty("mobile.page.height"));
    }
}
