package com.cloudmore.environment.api;

import com.cloudmore.environment.AbstractLocalFileEnvironment;
import com.cloudmore.environment.ApiEnvironment;

import java.io.IOException;

/**
 * @author sercansensulun on 18.10.2021.
 */
public class LocalFileAPIEnvironment extends AbstractLocalFileEnvironment implements ApiEnvironment {

    public LocalFileAPIEnvironment(String filePath) throws IOException {
        super.loadProperties(filePath);
    }

    @Override
    public String getApiBaseURL() {
        return properties.getProperty("api.url");
    }

    @Override
    public boolean isLogEnabled() {
        return Boolean.parseBoolean(properties.getProperty("log.enabled"));
    }
}
