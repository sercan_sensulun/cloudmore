package com.cloudmore.page.desktop;

import org.openqa.selenium.WebDriver;

/**
 * @author sercansensulun on 17.10.2021.
 */
public class DesktopPlatformPageObject extends DesktopBasePageObject {

    public DesktopPlatformPageObject(WebDriver webDriver) {
        super(webDriver);
    }

    @Override
    public void open() {
        webDriver.get("https://web.cloudmore.com/product");
    }

}
