package com.cloudmore.page.desktop;

import com.cloudmore.component.desktop.DesktopContactUsComponentObject;
import org.openqa.selenium.WebDriver;

/**
 * @author sercansensulun on 17.10.2021.
 */
public class DesktopContactUsPageObject extends DesktopBasePageObject {

    private DesktopContactUsComponentObject desktopContactUsComponentObject;

    public DesktopContactUsPageObject(WebDriver webDriver) {
        super(webDriver);
    }

    @Override
    public void open() {
        webDriver.get("https://web.cloudmore.com/contact-us");
    }

    @Override
    protected void initComponents(WebDriver webDriver) {
        super.initComponents(webDriver);
        desktopContactUsComponentObject = new DesktopContactUsComponentObject(webDriver);
    }

    public boolean isBodyContactUsFormVisible() {
        return desktopContactUsComponentObject.isEmailFieldVisible() &&
                desktopContactUsComponentObject.isFirstNameFieldVisible() &&
                desktopContactUsComponentObject.isLastNameFieldVisible() &&
                desktopContactUsComponentObject.isMessageFieldVisible() &&
                desktopContactUsComponentObject.isSubmitButtonVisible();
    }

    public boolean isFooterContactUsFormVisible() {
        return desktopFooterComponent.isEmailFieldVisible() &&
                desktopFooterComponent.isFirstNameFieldVisible() &&
                desktopFooterComponent.isLastNameFieldVisible() &&
                desktopFooterComponent.isMessageFieldVisible() &&
                desktopFooterComponent.isSubmitButtonVisible();
    }
}
