package com.cloudmore.page.desktop;

import com.cloudmore.component.desktop.DesktopSearchResultBodyComponentObject;
import com.cloudmore.data.ui.ResultItem;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebDriver;

import java.util.ArrayList;
import java.util.List;

/**
 * @author sercansensulun on 17.10.2021.
 */
@Slf4j
public class DesktopSearchResultPageObject extends DesktopBasePageObject {

    private DesktopSearchResultBodyComponentObject desktopSearchResultBodyComponentObject;

    public DesktopSearchResultPageObject(WebDriver webDriver) {
        super(webDriver);
    }

    @Override
    protected void initComponents(WebDriver webDriver) {
        super.initComponents(webDriver);
        desktopSearchResultBodyComponentObject = new DesktopSearchResultBodyComponentObject(webDriver);
    }

    @Override
    public void open() {
        webDriver.get("https://web.cloudmore.com/hs-search-results");
    }

    public List<ResultItem> getResultItems() {
        List<ResultItem> resultItemList = new ArrayList<>();
        int pageCount = 1;
        while (desktopSearchResultBodyComponentObject.isNextPageButtonDisplayed()) {
            if (pageCount == 3) {
                desktopSearchResultBodyComponentObject.takeScreenShot("desktop/search");
            }
            resultItemList.addAll(desktopSearchResultBodyComponentObject.getResultItems());
            desktopSearchResultBodyComponentObject.clickNextPageButton();
            pageCount++;
        }
        resultItemList.addAll(desktopSearchResultBodyComponentObject.getResultItems());
        log.info(String.format("Page count is %d.", pageCount));
        if (pageCount < 3) {
            desktopSearchResultBodyComponentObject.takeScreenShot("desktop/search");
        }
        return resultItemList;
    }
}
