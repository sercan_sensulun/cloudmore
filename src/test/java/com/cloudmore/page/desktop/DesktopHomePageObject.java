package com.cloudmore.page.desktop;

import com.cloudmore.component.desktop.DesktopSearchComponentObject;
import org.openqa.selenium.WebDriver;

/**
 * @author sercansensulun on 17.10.2021.
 */
public class DesktopHomePageObject extends DesktopBasePageObject {

    public DesktopHomePageObject(WebDriver webDriver) {
        super(webDriver);
    }

    @Override
    public void open() {
        webDriver.get("https://web.cloudmore.com/");
    }

    public boolean isPlatformMenuItemVisible() {
        return this.desktopHeaderComponent.isPlatformMenuItemVisible();
    }

    public boolean isSolutionMenuItemVisible() {
        return this.desktopHeaderComponent.isSolutionsMenuItemVisible();
    }


    public boolean isAboutUsMenuItemVisible() {
        return this.desktopHeaderComponent.isSolutionsMenuItemVisible();
    }

    public boolean isContactUsMenuItemVisible() {
        return this.desktopHeaderComponent.isContactUsMenuItemVisible();
    }

    public boolean isBlogMenuItemVisible() {
        return this.desktopHeaderComponent.isBlogMenuItemVisible();
    }

    public boolean isCaseStudiesMenuItemVisible() {
        return this.desktopHeaderComponent.isCaseStudiesMenuItemVisible();
    }

    public DesktopSearchResultPageObject search(String keyword) {
        DesktopSearchComponentObject desktopSearchComponentObject = this.desktopHeaderComponent.clickSearchButton();
        desktopSearchComponentObject.enterSearchKeyword(keyword);
        return desktopSearchComponentObject.clickSearchButton();
    }
}
