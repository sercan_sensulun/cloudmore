package com.cloudmore.page.desktop;

import com.cloudmore.component.desktop.DesktopFooterComponentObject;
import com.cloudmore.component.desktop.DesktopHeaderComponentObject;
import com.cloudmore.page.common.BasePageObject;
import org.openqa.selenium.WebDriver;

/**
 * @author sercansensulun on 17.10.2021.
 */
public abstract class DesktopBasePageObject extends BasePageObject {

    protected DesktopFooterComponentObject desktopFooterComponent;

    protected DesktopHeaderComponentObject desktopHeaderComponent;

    public DesktopBasePageObject(WebDriver webDriver) {
        super(webDriver);
    }

    @Override
    protected void initComponents(WebDriver webDriver) {
        desktopFooterComponent = new DesktopFooterComponentObject(webDriver);
        desktopHeaderComponent = new DesktopHeaderComponentObject(webDriver);
    }

    @Override
    public boolean isCompanyLogoVisible() {
        return desktopHeaderComponent.isCompanyLogoVisible();
    }

    public DesktopPlatformPageObject clickPlatformMenuItem() {
        return desktopHeaderComponent.clickPlatformMenuItem();
    }

    public DesktopSolutionPageObject clickSolutionMenuItem() {
        return desktopHeaderComponent.clickSolutionsMenuItem();
    }

    public DesktopAboutUsPageObject clickAboutUsMenuItem() {
        return desktopHeaderComponent.clickAboutUsMenuItem();
    }

    public DesktopContactUsPageObject clickContactUsMenuItem() {
        return desktopHeaderComponent.clickContactUsMenuItem();
    }

    public DesktopBlogPageObject clickBlogMenuItem() {
        return desktopHeaderComponent.clickBlogMenuItem();
    }

    public DesktopCaseStudiesPageObject clickCaseStudiesMenuItem() {
        return desktopHeaderComponent.clickCaseStudiesMenuItem();
    }

}
