package com.cloudmore.page.common;

import org.openqa.selenium.WebDriver;

/**
 * @author sercansensulun on 17.10.2021.
 */
public abstract class BasePageObject {

    protected WebDriver webDriver;

    public BasePageObject(WebDriver webDriver) {
        this.webDriver = webDriver;
        this.initComponents(webDriver);
    }

    protected abstract void initComponents(WebDriver webDriver);

    public String getURL(){
        return webDriver.getCurrentUrl();
    }

    public abstract void open();

    public abstract boolean isCompanyLogoVisible();
}
