package com.cloudmore.page.mobile;

import com.cloudmore.component.mobile.MobileSearchResultBodyComponentObject;
import com.cloudmore.data.ui.ResultItem;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebDriver;

import java.util.ArrayList;
import java.util.List;

/**
 * @author sercansensulun on 17.10.2021.
 */
@Slf4j
public class MobileSearchResultPageObject extends MobileBasePage {

    private MobileSearchResultBodyComponentObject mobileSearchResultBodyComponentObject;

    public MobileSearchResultPageObject(WebDriver webDriver) {
        super(webDriver);
    }

    @Override
    public void open() {

    }

    @Override
    protected void initComponents(WebDriver webDriver) {
        super.initComponents(webDriver);
        this.mobileSearchResultBodyComponentObject = new MobileSearchResultBodyComponentObject(webDriver);
    }

    public List<ResultItem> getResultItems() {
        List<ResultItem> resultItemList = new ArrayList<>();
        int pageCount = 1;
        while (mobileSearchResultBodyComponentObject.isNextPageButtonDisplayed()) {
            if (pageCount == 3) {
                mobileSearchResultBodyComponentObject.takeScreenShot("mobile/search");
            }
            resultItemList.addAll(mobileSearchResultBodyComponentObject.getResultItems());
            mobileSearchResultBodyComponentObject.clickNextPageButton();
            pageCount++;
        }
        resultItemList.addAll(mobileSearchResultBodyComponentObject.getResultItems());
        log.info(String.format("Page count is %d.", pageCount));
        if (pageCount < 3) {
            mobileSearchResultBodyComponentObject.takeScreenShot("mobile/search");
        }
        return resultItemList;
    }
}
