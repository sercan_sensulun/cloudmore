package com.cloudmore.page.mobile;

import com.cloudmore.component.mobile.MobileFooterComponentObject;
import com.cloudmore.component.mobile.MobileHeaderComponentObject;
import com.cloudmore.page.common.BasePageObject;
import org.openqa.selenium.WebDriver;

/**
 * @author sercansensulun on 17.10.2021.
 */
public abstract class MobileBasePage extends BasePageObject {

    protected MobileHeaderComponentObject mobileHeaderComponent;

    protected MobileFooterComponentObject mobileFooterComponent;

    public MobileBasePage(WebDriver webDriver) {
        super(webDriver);
        initComponents(webDriver);
    }

    @Override
    protected void initComponents(WebDriver webDriver) {
        mobileFooterComponent = new MobileFooterComponentObject(webDriver);
        mobileHeaderComponent = new MobileHeaderComponentObject(webDriver);
    }

    @Override
    public boolean isCompanyLogoVisible() {
        return false;
    }
}
