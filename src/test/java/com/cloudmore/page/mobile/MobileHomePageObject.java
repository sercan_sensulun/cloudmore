package com.cloudmore.page.mobile;

import com.cloudmore.component.mobile.MobileSearchComponentObject;
import org.openqa.selenium.WebDriver;

/**
 * @author sercansensulun on 17.10.2021.
 */
public class MobileHomePageObject extends MobileBasePage {

    public MobileHomePageObject(WebDriver webDriver) {
        super(webDriver);
    }

    @Override
    public void open() {
        webDriver.get("https://web.cloudmore.com/");
    }

    public MobileSearchResultPageObject search(String keyword) {
        MobileSearchComponentObject mobileSearchComponent = this.mobileHeaderComponent.clickSearchButton();
        mobileSearchComponent.enterSearchKeyword(keyword);
        return mobileSearchComponent.clickSearchButton();
    }
}
