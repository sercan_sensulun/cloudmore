# new feature
# Tags: optional
@mobile
@search_mobile

Feature: Search by keyword works on mobile Home Page.

  Scenario Outline: Search by <keyword> keyword works on mobile Home Page.
    Given mobile home page is opened
    When "<keyword>" keyword is searched on mobile home page
    Then each result has "<keyword>" keyword on mobile search result page
    Examples:
      | keyword   |
      | Högset    |
      | cloudmore |
      | foo       |