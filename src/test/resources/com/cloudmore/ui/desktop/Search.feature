# new feature
# Tags: optional
@desktop
@search_desktop

Feature: Search by keyword works on desktop Home Page.

  Scenario Outline: Search by <keyword> keyword works on Home Page.
    Given desktop home page is opened
    When "<keyword>" keyword is searched on desktop home page
    Then each result has "<keyword>" keyword on desktop search result page
    Examples:
      | keyword   |
      | Högset    |
      | cloudmore |
      | foo       |