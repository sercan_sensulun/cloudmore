# new feature
# Tags: optional
@desktop
@company_logo_desktop

Feature: Cloudmore home page has company logo. Every next page: Platform, Solution, About us, Contact us, Blog, Case studies have company logo.

  Scenario: Desktop home page has company logo.
    When desktop home page is opened
    Then company logo is shown on desktop home page

  Scenario: Desktop platform page has company logo.
    Given desktop home page is opened
    When platform menu item is clicked on desktop home page
    And desktop platform page is opened successfully with "https://web.cloudmore.com/product" url
    Then company logo is shown on desktop platform page

  Scenario: Desktop solution page has company logo.
    Given desktop home page is opened
    When solution menu item is clicked on desktop home page
    And desktop solution page is opened successfully with "https://web.cloudmore.com/solutions" url
    Then company logo is shown on desktop solution page

  Scenario: Desktop about us page has company logo.
    Given desktop home page is opened
    When about us menu item is clicked on desktop home page
    And desktop about us page is opened successfully with "https://web.cloudmore.com/about-us" url
    Then company logo is shown on desktop about us page

  Scenario: Desktop contact us page has company logo.
    Given desktop home page is opened
    When contact us menu item is clicked on desktop home page
    And desktop contact us page is opened successfully with "https://web.cloudmore.com/contact-us" url
    Then company logo is shown on desktop contact us page

  Scenario: Desktop blog page has company logo.
    Given desktop home page is opened
    When blog menu item is clicked on desktop home page
    And desktop blog page is opened successfully with "https://web.cloudmore.com/blog" url
    Then company logo is shown on desktop blog page

  Scenario: Desktop case studies page has company logo.
    Given desktop home page is opened
    When case studies menu item is clicked on desktop home page
    And desktop case studies page is opened successfully with "https://web.cloudmore.com/case-studies" url
    Then company logo is shown on desktop case studies page
