# new feature
# Tags: optional
@desktop
@contact_us_desktop

Feature: Page Contact us has ‘Contact us’ form as in body of page, as well in footer.

  Scenario: Page Contact us has ‘Contact us’ form as in body of page
    When desktop contact us page is opened
    Then Contact us form is shown in body of desktop contact us page

  Scenario: Page Contact us has 'Contact us' form in footer of page
    When desktop contact us page is opened
    Then Contact us form is shown in footer of desktop contact us page