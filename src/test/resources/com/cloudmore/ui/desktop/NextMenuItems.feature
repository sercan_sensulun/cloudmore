# new feature
# Tags: optional
@desktop
@next_menu_items_desktop

Feature: Cloudmore home page has next menu items: Platform, Solution, About us, Contact us, Blog, Case studies. 

  Scenario: Desktop home page has next menu item which is Platform.
    When desktop home page is opened
    Then platform menu item is shown on desktop home page

  Scenario: Desktop home page has next menu items which is Solution.
    When desktop home page is opened
    Then solution menu item is shown on desktop home page

  Scenario: Desktop home page has next menu items which is About Us.
    When desktop home page is opened
    Then about us menu item is shown on desktop home page

  Scenario: Desktop home page has next menu items which is Contact Us.
    When desktop home page is opened
    Then contact us menu item is shown on desktop home page

  Scenario: Desktop home page has next menu items which is Blog.
    When desktop home page is opened
    Then blog menu item is shown on desktop home page

  Scenario: Desktop home page has next menu items which is Case Studies.
    When desktop home page is opened
    Then case studies menu item is shown on desktop home page