# new feature
# Tags: optional
@api
Feature: User can be deleted over API.

  Background:
    Given there is user with "test" username

  #assume that special char not supported in username
  Scenario: Invalid username supplied message is returned with 400 bad request if invalid username is entered.
    When "şişişi" username is entered to delete via API
    Then "400" response code is returned in delete response

  Scenario: User not found message is returned with 404 not found if non-exist user is entered.
    When non-exist username is entered to delete via API
    Then "404" response code is returned in delete response

  Scenario: User is deleted successfully if user exist.
    When exist username is entered to delete on via API
    Then "200" response code is returned in delete response