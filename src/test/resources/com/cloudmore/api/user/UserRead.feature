# new feature
# Tags: optional
@api
Feature: User can be read over API.

  Background:
    Given there is user with "test" username

  #assume that special char not supported in username
  Scenario: Invalid username supplied message is returned with 400 bad request if invalid username is entered.
    When "şişişi" username is entered to get user via API
    Then "400" response code is returned in get response

  Scenario: User not found message is returned with 404 not found if non-exist user is entered.
    When non-exist username is entered to get user via API
    Then "404" response code is returned in get response

  Scenario: User is returned successfully
    When "test" username is entered to get user via API
    Then "200" response code is returned in get response
    And username is returned in get response
    And first name is returned in get response
    And last name is returned in get response