# new feature
# Tags: optional
@api
Feature: User can be updated over API.

  Background:
    Given there is user with "test" username

  #assume that special char not supported in username
  Scenario: Invalid username supplied message is returned with 400 bad request if invalid username is entered.
    When "şişiş" username is entered to update user via API
    Then "400" response code is returned in update response

  Scenario: User not found message is returned with 404 not found if non-exist user is entered.
    When non-exist username is entered to update user via API
    Then "404" response code is returned in update response

  Scenario: Password should be strong while updating it.
    When "Ss123*_" password is entered
    When "test" username is entered to update user via API
    Then "400" response code is returned in update response

  Scenario: Password is updated if it is strong.
    When "Ss1234*_" password is entered
    When "test" username is entered to update user via API
    Then "200" response code is returned in update response

  Scenario: first name is updated successfully.
    When "firstnametest1" first name is entered
    And "test" username is entered to update user via API
    Then "200" response code is returned in update response

