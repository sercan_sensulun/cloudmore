# new feature
# Tags: optional
@api
Feature: User can be created over API.

  #strong password: min 8 char, include number, lower-case, upper-case, special-char
  Scenario Outline: Password is should be strong while creating user over API.
    Given "<username>" username is entered
    And "<firstName>" first name is entered
    And "<lastName>" last name is entered
    And "<email>" e-mail is entered
    And "<phone>" phone number is entered
    And "<password>" password is entered
    When user is tried to be added via API
    Then "<responseCode>" response code is returned in add response
    And "<responseMessage>" message is returned in add response
    Examples:
      | password   | responseCode | responseMessage              | username      | firstName      | lastName      | email            | phone        |
      | 12345678   | 400          | please enter strong password | testusername1 | testfirstname1 | testlastname1 | test@yopmail.com | 905350000000 |
      | 12345678a  | 400          | please enter strong password | testusername1 | testfirstname1 | testlastname1 | test@yopmail.com | 905350000000 |
      | 12345678A  | 400          | please enter strong password | testusername1 | testfirstname1 | testlastname1 | test@yopmail.com | 905350000000 |
      | 12345678*  | 400          | please enter strong password | testusername1 | testfirstname1 | testlastname1 | test@yopmail.com | 905350000000 |
      | 12345678aB | 400          | please enter strong password | testusername1 | testfirstname1 | testlastname1 | test@yopmail.com | 905350000000 |
      | 12345678A* | 400          | please enter strong password | testusername1 | testfirstname1 | testlastname1 | test@yopmail.com | 905350000000 |
      | 1231231*a  | 400          | please enter strong password | testusername1 | testfirstname1 | testlastname1 | test@yopmail.com | 905350000000 |
      | 1A*a123    | 400          | please enter strong password | testusername1 | testfirstname1 | testlastname1 | test@yopmail.com | 905350000000 |

  Scenario: Username should be unique while creating user over API.
    Given there is user with "test" username
    And "test" username is entered
    And "testFirstName" first name is entered
    And "testLastName" last name is entered
    And "test123@yopmail.com" e-mail is entered
    And "905350000000" phone number is entered
    And "123Ser*_" password is entered
    When user is tried to be added via API
    Then "400" response code is returned in add response
    And "user already exist" message is returned in add response

  Scenario Outline: User is created with valid parameters over API.
    Given "<username>" username is entered
    And "<firstName>" first name is entered
    And "<lastName>" last name is entered
    And "<email>" e-mail is entered
    And "<phone>" phone number is entered
    And "<password>" password is entered
    When user is tried to be added via API
    Then "200" response code is returned in add response
    Examples:
      | username | firstName  | lastName  | email             | phone        | password |
      | test1    | firstname1 | lastname1 | test1@yopmail.com | 905350000000 | Ss123*_* |